<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ChinesewushurankingTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ChinesewushurankingTable Test Case
 */
class ChinesewushurankingTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ChinesewushurankingTable
     */
    public $Chinesewushuranking;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Chinesewushuranking'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Chinesewushuranking') ? [] : ['className' => ChinesewushurankingTable::class];
        $this->Chinesewushuranking = TableRegistry::getTableLocator()->get('Chinesewushuranking', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Chinesewushuranking);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
