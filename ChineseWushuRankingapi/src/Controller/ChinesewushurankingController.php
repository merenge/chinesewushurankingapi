<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Chinesewushuranking Controller
 *
 * @property \App\Model\Table\ChinesewushurankingTable $Chinesewushuranking
 *
 * @method \App\Model\Entity\Chinesewushuranking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ChinesewushurankingController extends AppController
{
    public function index(){

        



        // $aaa=mysql_query($sql);

        // $rank = 1;
        // $rank_count = 0;
        // $lastscore = null;

        // while($bbb=mysql_fetch_array($aaa)){
        // $rank_count++;
        // if ($lastscore !== $bbb["score"]) {
        // $rank = $rank_count;
        // $lastscore = $bbb["score"];
        // }

        // echo $bbb["sub"];
        // echo $bbb["score"];
        // //echo $bbb["順位"];
        // echo $rank;
        // }


    }


    public function vote()
    {
        

        //voteアクション内で特に処理を行わないので記述なし
        // $this->viewBuilder()->antoLayout(false);
        // $this->set('title', '')

        // if($this->request->isPost()) {
        //     $this->set('data', $this->request->data['result']);
        // } else {
        //     $this->set('data', []);
        // }

        // Array
        // (
        //     [select] =>Array
        //         (
        //             [0] = one
        //             [1] = two
        //             [2] = three
        //         )
        // );

        // if($this->request->is('post')){
        //     $find = $this->request->data['People']['find'];
        //     $condition = ['conditions'=>['name', $find]];
        //     $data = $this->People->find('all', $condition);
        // } else {
        //     $data = $this->People->find('all');
        // }
        // $this->set('data', $data);


        $query00 = $this->Chinesewushuranking->find('all');
        $query00->order(['Chinesewushuranking. Popularity' => 'DESC']);
        $query00->limit(3);
        $searchValue = $query00->toArray();

        for($index = 0; $index < 3; $index ++){
            $search[$index] = $searchValue[$index]['Name']."：".$searchValue[$index]['Popularity']."票";
        }
        $this->set('searchValue0', $search[0]);
        $this->set('searchValue1', $search[1]);
        $this->set('searchValue2', $search[2]);
    }

    public function result()
    {
        $query00 = $this->Chinesewushuranking->find('all');
        $query00->order(['Chinesewushuranking. Popularity' => 'DESC']);
        $query00->limit(3);
        $testValue = $query00->toArray();

        for($index = 0; $index < 3; $index ++){
            $test[$index] = $testValue[$index]['Name']."：".$testValue[$index]['Popularity']."票";
        }
        $this->set('testValue0', $test[0]);
        $this->set('testValue1', $test[1]);
        $this->set('testValue2', $test[2]);



        //Postデータの取得
        $postData = $this->request->getData('id');

        //レコードの更新
        $voteSystemTable = $this->getTableLocator()->get('Chinesewushuranking'); //votesystemテーブルの取得
        $tarrgetRecord = $voteSystemTable->get($postData);              //votesystemテーブルから該当IDのレコードを取得
        $tarrgetRecord->Popularity = $tarrgetRecord['Popularity'] + 1;  //レコードのPopularityを更新
        $voteSystemTable->save($tarrgetRecord);                         //更新したレコードの保存
        
        //レコード件数取得
        $recordNum = $this->Chinesewushuranking->find()->count();
        
        //全レコードのPopularityを取得
        for ($i=1; $i <= $recordNum; $i++)
        {
            $tmpRecord = $voteSystemTable->get($i);             //Idが1から順番にレコードを取得
            $this->set('vote'.$i, $tmpRecord['Popularity']);    //該当レコードのPopularityをViewに渡す

            // $query = $this->Chinesewushuranking->find('all');   //[chinesewushuranking]テーブルからクエリを取得->select(['Vete'])->where(['Popularity' => (int)$mostPopularity->mostPopularity])->toArray()
            // // ※ここでクエリを利用してデータの並べ替えなどが行える。
            // // debug($query); //現在のクエリの状態をデバッグ表示する

            // // $array("id", ['1'=> '査拳', '2' => '太極拳', '3' => '八卦掌', '4'=> '八極拳', '5' => '形意拳', '6' => '酔拳', '7' => '少林拳','8'=> '武当玄武拳', '9' => '蟷螂拳', '10' => '心意六合拳']);
            
            // $query->order(['id' => 'ASC']); //カラム['id']をキーにして昇順ソート
            // $query->limit(3); //３個に絞る
            // //クエリを実行してarrayにデータを格納
            // $array = $query->toArray();
        }
        
        //人気トップを抽出
        $mostPopularity = $this->Chinesewushuranking->find()->select(['mostPopularity' => $this->Chinesewushuranking->find()->func()->max('Popularity')])->first();   //Popularityの最大値を取得
        // $mostPopularityVote = $this->Chinesewushuranking->find()->select(['mostPopularityvte' => $this->Chinesewushuranking->find()->func()->max('PopularityVete')])->first();   //Popularityの最大値を取得

        // $query = $this->Chinesewushuranking->find('all');   //[chinesewushuranking]テーブルからクエリを取得->select(['Vete'])->where(['Popularity' => (int)$mostPopularity->mostPopularity])->toArray()
        

        $ret = $this->Chinesewushuranking->find('all')->select(['Name'])->where(['Popularity' => (int)$mostPopularity->mostPopularity])->toArray();               //Popularityの最大値と同じレコードを抽出して配列に格納
        $ret2 = $this->Chinesewushuranking->find()->select(['Popularity'])->where(['Popularity' => (int)$mostPopularity->mostPopularity])->toArray();
        
        //Popularityがトップの名前の表示用の文字列を用意
        $mostPopularityNames = "";
        for ($j=0; $j < count($ret); $j++)
        { 
            $mostPopularityNames = $mostPopularityNames.$ret[$j]['Name']."だよー！ ";

        }

        $this->set('mostPopularity', $mostPopularityNames); //人気トップのNameをViewに渡す

        //Popularityがトップのvoteの表示用の文字列を用意
        $mostPopularityVotes = "";
        for ($j=0; $j < count($ret2); $j++)
        { 
            $mostPopularityVotes = $mostPopularityVotes.$ret2[$j]['Popularity']."票";
        }
        
        $this->set('mostPopularityVote', $mostPopularityVotes); //人気トップのPopularityをViewに渡す


        // $this->Chinesewushuranking->find()
        //     ->where(['Chinesewushuranking.name' => '太郎'])
        //     ->order(['Chinesewushuranking.name' => 'ASC'])
        //     ->order(['Chinesewushuranking.id' => 'DESC']);

        // // SQL
        // WHERE "Chinesewushuranking"."name" = '太郎' ORDER BY "Chinesewushuranking"."name" ASC, "Chinesewushuranking"."id" DESC



        // $res = $this->Rate->query('SELECT Rate1.Chinesewushuranking_id , sum(Rate1.rate) as Popularity   
        //                 FROM ageha_rates AS Rate1  
        //                 GROUP BY Rate1.Chinesewushuranking_id  
        //                 HAVING Rate1.Chinesewushuranking_id ORDER BY Popularity desc LIMIT 5  
        //            ');  

        //                   Rate1 rate

        // [chinesewushuranking]テーブルからクエリを取得
        // $query = $this->Chinesewushuranking->find('all');

                // ※ここでクエリを利用してデータの並べ替えなどが行える。
                // debug($query); //現在のクエリの状態をデバッグ表示する

                // $query->where(['id' => 1]); //カラム['id']に[1]が入っているもののみに絞る。
                // $query->order(['id' => 'ASC']); //カラム['id']をキーにして昇順ソート
                // $query->order(['id' => 'DESC']); //カラム['id']をキーにして降順ソート
                // $query->limit(3); //３個に絞る

        

        // [chinesewushuranking]コントローラーで[user]モデルを利用できるようにする。
        // $this->loadModel('user');
        // $query2 = $this->Chinesewushuranking->find('all');
        // $array2 = $query->toArray();

        // 基本的な使い方
        // $this->Chinesewushuranking->find()
                
        //     ->order(['id' => 'ASC'])

        //     ->all()
    


        // 結果➡http://localhost/ChineseWushuRanking/chinesewushuranking/vote
    }

    public function getMessages(){
		error_log("getMessagess()");
		
        $this->autoRender = false;
        
        $query = $this->Chinesewushuranking->find('all');
        
        $json_array = json_encode($query);
        
		echo $json_array;
    }
    
    public function setMessage(){
        
        error_log("setMessage()");
        //---------------
        // Viewのレンダーを無効化
        // これを行う事で対になるテンプレート(.tpl)が不要となる。
        $this->autoRender = false;
        //---------------
        // POSTデータの受け取り方
        // name,message をPOSTで受け取る。
        $name = "";
        if( isset( $this->request->data['name'] ) ){
            $name   = $this->request->data['name'];
            error_log($name);
        }
        $popularity = "";
        if( isset( $this->request->data['popularity'] ) ){
            $popularity    = $this->request->data['popularity'];
            error_log($popularity);
        }
        //---------------
        //テーブルに追加するレコード情報を作る
        // $data   = array ( 'Name' => $name, 'Message' => $message, 'Date' => date('Y/m/d') );   //Date型
        $data   = array ( 'Name' => $name, 'Popularity' => $popularity); //timestamp型, 'Date' => date('Y/m/d H:i:s')
        $chinesewushuranking = $this->Chinesewushuranking->newEntity();
        $chinesewushuranking = $this->Chinesewushuranking->patchEntity($chinesewushuranking, $data);
        // $chinesewushuranking = $this->Chinesewushuranking->patchEntity($chinesewushuranking, $data, ['validate' => false]);    //バリデート無し
        if ($this->Chinesewushuranking->save($chinesewushuranking)) {
            //追加成功
            echo "success!"; //success!
        }else{
            //追加失敗
            echo "failed!"; //failed!
        }
    }
}



/* vote.ctpより

// <!-- <?php foreach($data->toArray() as $obj): ?> -->
<!-- <tr>
 <!-- <td><?=h($obj->id) ?></td> -->
   <!-- S<td><a href="<?=$this->Url->build(['controller'=>'Chinesewushuranking', 'action' => 'edit']); ?>?id=<?=$obj->id ?>"> -->
       <!-- <?=h($obj->name) ?></a></td> -->
   <!-- <td><a href="<?=$this->Url->build(['controller'=>'Chinesewushuranking', 'action' => 'delete']); ?>?id=<?=$obj->id ?>">delete</a></td> -->
   </tr> -->
   <!-- <?php endforeach; ?> --> */




// <?php
// namespace App\Controller;

// use App\Controller\AppController;

// /**
//  * Chinesewushuranking Controller
//  *
//  * @property \App\Model\Table\ChinesewushurankingTable $Chinesewushuranking
//  *
//  * @method \App\Model\Entity\Chinesewushuranking[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
//  */
// class ChinesewushurankingController extends AppController
// {
//     /**
//      * Index method
//      *
//      * @return \Cake\Http\Response|null
//      */
//     public function index()
//     {
//         $chinesewushuranking = $this->paginate($this->Chinesewushuranking);

//         $this->set(compact('chinesewushuranking'));
//     }

//     /**
//      * View method
//      *
//      * @param string|null $id Chinesewushuranking id.
//      * @return \Cake\Http\Response|null
//      * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
//      */
//     public function view($id = null)
//     {
//         $chinesewushuranking = $this->Chinesewushuranking->get($id, [
//             'contain' => []
//         ]);

//         $this->set('chinesewushuranking', $chinesewushuranking);
//     }

//     /**
//      * Add method
//      *
//      * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
//      */
//     public function add()
//     {
//         $chinesewushuranking = $this->Chinesewushuranking->newEntity();
//         if ($this->request->is('post')) {
//             $chinesewushuranking = $this->Chinesewushuranking->patchEntity($chinesewushuranking, $this->request->getData());
//             if ($this->Chinesewushuranking->save($chinesewushuranking)) {
//                 $this->Flash->success(__('The chinesewushuranking has been saved.'));

//                 return $this->redirect(['action' => 'index']);
//             }
//             $this->Flash->error(__('The chinesewushuranking could not be saved. Please, try again.'));
//         }
//         $this->set(compact('chinesewushuranking'));
//     }

//     /**
//      * Edit method
//      *
//      * @param string|null $id Chinesewushuranking id.
//      * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
//      * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
//      */
//     public function edit($id = null)
//     {
//         $chinesewushuranking = $this->Chinesewushuranking->get($id, [
//             'contain' => []
//         ]);
//         if ($this->request->is(['patch', 'post', 'put'])) {
//             $chinesewushuranking = $this->Chinesewushuranking->patchEntity($chinesewushuranking, $this->request->getData());
//             if ($this->Chinesewushuranking->save($chinesewushuranking)) {
//                 $this->Flash->success(__('The chinesewushuranking has been saved.'));

//                 return $this->redirect(['action' => 'index']);
//             }
//             $this->Flash->error(__('The chinesewushuranking could not be saved. Please, try again.'));
//         }
//         $this->set(compact('chinesewushuranking'));
//     }

//     /**
//      * Delete method
//      *
//      * @param string|null $id Chinesewushuranking id.
//      * @return \Cake\Http\Response|null Redirects to index.
//      * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
//      */
//     public function delete($id = null)
//     {
//         $this->request->allowMethod(['post', 'delete']);
//         $chinesewushuranking = $this->Chinesewushuranking->get($id);
//         if ($this->Chinesewushuranking->delete($chinesewushuranking)) {
//             $this->Flash->success(__('The chinesewushuranking has been deleted.'));
//         } else {
//             $this->Flash->error(__('The chinesewushuranking could not be deleted. Please, try again.'));
//         }

//         return $this->redirect(['action' => 'index']);
//     }
// }
