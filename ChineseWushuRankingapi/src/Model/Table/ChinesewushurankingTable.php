<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Chinesewushuranking Model
 *
 * @method \App\Model\Entity\Chinesewushuranking get($primaryKey, $options = [])
 * @method \App\Model\Entity\Chinesewushuranking newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Chinesewushuranking[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Chinesewushuranking|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Chinesewushuranking saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Chinesewushuranking patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Chinesewushuranking[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Chinesewushuranking findOrCreate($search, callable $callback = null, $options = [])
 */
class ChinesewushurankingTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('chinesewushuranking');
        $this->setDisplayField('Id');
        $this->setPrimaryKey('Id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('Id')
            ->allowEmptyString('Id', null, 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 30)
            ->requirePresence('Name', 'create')
            ->notEmptyString('Name');

        $validator
            ->integer('Popularity')
            ->requirePresence('Popularity', 'create')
            ->notEmptyString('Popularity');

        return $validator;
    }
}
