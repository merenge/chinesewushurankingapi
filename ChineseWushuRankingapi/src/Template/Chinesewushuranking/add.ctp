<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Chinesewushuranking $chinesewushuranking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Chinesewushuranking'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="chinesewushuranking form large-9 medium-8 columns content">
    <?= $this->Form->create($chinesewushuranking) ?>
    <fieldset>
        <legend><?= __('Add Chinesewushuranking') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('Popularity');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
