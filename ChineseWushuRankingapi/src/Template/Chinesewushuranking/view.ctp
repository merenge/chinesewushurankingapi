<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Chinesewushuranking $chinesewushuranking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Chinesewushuranking'), ['action' => 'edit', $chinesewushuranking->Id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Chinesewushuranking'), ['action' => 'delete', $chinesewushuranking->Id], ['confirm' => __('Are you sure you want to delete # {0}?', $chinesewushuranking->Id)]) ?> </li>
        <li><?= $this->Html->link(__('List Chinesewushuranking'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Chinesewushuranking'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="chinesewushuranking view large-9 medium-8 columns content">
    <h3><?= h($chinesewushuranking->Id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($chinesewushuranking->Name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($chinesewushuranking->Id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Popularity') ?></th>
            <td><?= $this->Number->format($chinesewushuranking->Popularity) ?></td>
        </tr>
    </table>
</div>
